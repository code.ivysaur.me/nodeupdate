# nodeupdate

![](https://img.shields.io/badge/written%20in-Bash-blue)

Shell script to update node.js binaries from nodejs.org.

Originally bundled with opencommandio - reproduced here for easy standalone usage.

## Usage

 `./nodeupdate.sh`

Tags: sysadmin


## Download

- [⬇️ nodeupdate-0_10_26-v2.sh](dist-archive/nodeupdate-0_10_26-v2.sh) *(2.25 KiB)*
- [⬇️ nodeupdate-0_10_26-v1.sh](dist-archive/nodeupdate-0_10_26-v1.sh) *(2.25 KiB)*
- [⬇️ nodeupdate-0_10_21.sh](dist-archive/nodeupdate-0_10_21.sh) *(2.25 KiB)*
