#!/bin/bash

# Update node.js binaries from the nodejs.org linux binaries
# (Poor man's package management..)

#
# Configuration:
#

LATEST_NODE_VERSION="0.10.26"
SHA1_X64="d15d39e119bdcf75c6fc222f51ff0630b2611160"
SHA1_X86="b3bebee7f256644266fccce04f54e2825eccbfc0"

#
# Script
#

if [ ! "$UID" -eq 0 ] ; then
	echo "Please run this script as root." >&2
	exit 1
fi
cd /root/

## Determine architecture for download

ARCHITECTURE=`uname -m`

if [ "$ARCHITECTURE" = "x86_64" ] ; then
	TARNAME="node-v$LATEST_NODE_VERSION-linux-x64"
	SHA1_CHECK="$SHA1_X64"
elif [ "$ARCHITECTURE" = "i686" ] ; then
	TARNAME="node-v$LATEST_NODE_VERSION-linux-x86"
	SHA1_CHECK="$SHA1_X86"
else
	echo "Unknown architecture \"$ARCHITECTURE\"!"
	exit 1
fi

## Check current node installation

NODEPATH=`which node`
if [ $? -eq 0 ] ; then

	echo "Found node at $NODEPATH"
	CURRENT_VERSION=`$NODEPATH --version`
	
	if [ "$CURRENT_VERSION" = "v$LATEST_NODE_VERSION" ] ; then
		echo "Node is already the latest version."
		exit 0
	fi
	
	echo "Node is an older version, removing..."
	
	pkill node
	
	# Safely remove older version
	
	if [ `dirname "$NODEPATH"` = "/usr/local/bin" ] ; then
	
		echo "Removing node files from /usr/local/bin..."
		
		rm /usr/local/bin/node
		rm /usr/local/bin/npm
		rm /usr/local/lib/dtrace/node.d
		rm /usr/local/share/man/man1/node.1
		rm -r /usr/local/lib/node_modules
		# might exist if the old installation was built from source
		rm -r /usr/local/include/node 2>/dev/null
		
	else
		echo "Unknown node installation path, not removing..."
	fi
	
fi

## Download file

echo "Downloading..."
wget -q -O - "http://nodejs.org/dist/v${LATEST_NODE_VERSION}/${TARNAME}.tar.gz" > /root/${TARNAME}.tar.gz

if [ ! -s "/root/${TARNAME}.tar.gz" ] ; then
	echo "Download failed!" >&2
	exit 1
fi

DOWNLOADED_SHASUM=`sha1sum "/root/$TARNAME.tar.gz" | cut -d' ' -f 1`

if [ ! "$DOWNLOADED_SHASUM" = "$SHA1_CHECK" ] ; then
	echo "WARNING: Downloaded file has mismatching sha1 \"${DOWNLOADED_SHASUM}\"!" >&2
	rm "/root/${TARNAME}.tar.gz"
	exit 1
fi

## Unpack and extract

tar zxfp "/root/${TARNAME}.tar.gz"
cd "/root/${TARNAME}"

cp -rpf bin/* /usr/local/bin
cp -rpf lib/* /usr/local/lib
cp -rpf share/* /usr/local/share

cd ../
rm -r "/root/${TARNAME}"
rm "${TARNAME}.tar.gz"

echo "Node updated"
node --version

exit 0
